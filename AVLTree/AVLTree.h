#pragma once
#include <iostream>
#include <vector>
#include <stack>
#include<queue>
#include <algorithm>
using namespace std;

template <typename T>
class AVLTree
{
	/*public:
		typedef enum { BST_OK,
					   BST_OUTOFMEMORY,
					   BST_ALREADYEXISTS,
					   BST_DOESNTEXIST,
					   BST_FAILED,
					   BST_EMPTY } BST_ERROR;*/

 private:
	class Node
	{
	public:
		T     Data;
		Node *Left;
		Node *Right;

		Node(const T &Item)
			:Data(Item), Left(nullptr), Right(nullptr) {/**/}//Default constructor

		Node(const T &Item, Node *L, Node *R) noexcept
			:Data(Item), Left(L), Right(R) {/**/}//Constructor

		~Node() { Left = Right = nullptr; }//Destructor

		//--- AUXILIARY METHODS ---//
		inline bool IsLeafNode() const noexcept
		{
			return Left == nullptr && Right == nullptr;
		}//isLeafNode

		inline bool HasOneChild() const noexcept
		{
			return (Left == nullptr && Right != nullptr) || (Left != nullptr && Right == nullptr);
		}//HasOneChild

		inline bool HasTwoChildren() const noexcept
		{
			return (Left != nullptr && Right != nullptr);
		}//HasTwoChildren
	};//nodeClass

	Node  *Root;
	size_t NumNodes;

	//--- PRIVATE METHODS ---//
	inline bool DeleteTree(Node* node)noexcept;
	inline size_t Height(Node *node)const noexcept;
	inline size_t NodeCount(Node *node)const noexcept;
	inline void PreOrder(Node *node)const noexcept;
    inline void PostOrder(Node *node)const noexcept;
    inline void InOrder(Node *node)const noexcept;
	inline void LevelOrder(Node *node)const noexcept;
	inline  int BalanceFactor(Node *node)const noexcept; 
	inline void LeftLeft(Node* node) noexcept;
	inline void RightRight(Node* node) noexcept;
	inline void LeftRight(Node* node) noexcept;
	inline void RightLeft(Node* node) noexcept;
	inline bool AVLBalance(Node* node)noexcept;//Balances the AVL tree after we delete a node
	inline bool AVLBalance()noexcept;//Balances the AVL tree after we delete a node
	inline bool SearchNodeToRebalance(Node* node)noexcept;
	inline typename AVLTree<T>::Node* DeleteNode(Node* Root, T item)noexcept;
	inline typename AVLTree<T>::Node* MinValue(Node* Root)const noexcept;
	inline typename AVLTree<T>::Node* MaxValue(Node* Root)const noexcept;
 public:

	//--- PUBLIC METHODS ---//
	AVLTree()noexcept;//Default Constructor
	~AVLTree()noexcept;//Destructor
	AVLTree(const AVLTree<T> &other) noexcept;//Copy Constructor
	AVLTree<T> &operator= (const AVLTree<T> &other)noexcept;//Operator=
	AVLTree(AVLTree<T> &&other)noexcept;//Move constructor
	AVLTree<T> &operator= (const AVLTree<T> &&other)noexcept;//move operator

	inline bool Add(const T &newData)noexcept;//Add a new node 
	inline bool DeleteTree()noexcept;//Delete the whole Tree
	inline bool Compare(const AVLTree<T> &other) const noexcept;//Compare two trees
	inline bool operator==(const AVLTree<T> &other) const noexcept;//Compare two trees
	inline bool isEmpty()const noexcept;//Checks if Tree is Empty
	inline bool SearchNode(const T &Value)const noexcept;//Searches if a Node Exists
	inline bool DeleteNode(const T& data)noexcept;//Deletes a node if it exists
	inline size_t GetItemCount()const noexcept;//Returns the number of nodes inside the tree
	inline size_t Height()const noexcept;//Returns the height of the tree
	inline size_t NodeCount()const noexcept;//This method counts the number of nodes 
	inline void PreOrder()const noexcept;
	inline void PostOrder()const noexcept;
	inline void InOrder()const noexcept;
	inline void LevelOrder()const noexcept;
	inline std::vector<T> ToArray() const noexcept; //from a vector to a BST
	inline bool FromArray(const std::vector<T> &v) noexcept; //from a BST to a vector
	inline T MinValue()const noexcept;
	inline T MaxValue()const noexcept;
	inline bool isFull() const noexcept;
};//AVLTree

template<typename T>
AVLTree<T>::AVLTree()noexcept
 :Root(nullptr),NumNodes(0){/**/}//Default Constructor

template<typename T>
AVLTree<T>::~AVLTree() { DeleteTree(); }//Destructor

template<typename T>
AVLTree<T>::AVLTree(const AVLTree<T> &other)noexcept
{
	if (other.isEmpty())return;
	else
	{
		stack<Node *> nodeStack;
		nodeStack.push(other.Root);

		while (nodeStack.empty() == false)
		{
			Node *node = nodeStack.top();
			Add(node->Data);
			nodeStack.pop();

			if (node->Right)nodeStack.push(node->Right);
			if (node->Left)nodeStack.push(node->Left);

		}//if
	}//if
}//copy constructor

template<typename T>
AVLTree<T> &AVLTree<T>::operator=(const AVLTree<T> &other)noexcept
{
	if (other.isEmpty())return *this;
	else
	{
		stack<Node *> nodeStack;
		nodeStack.push(other.Root);

		while (nodeStack.empty() == false)
		{
			Node *node = nodeStack.top();

			Add(node->Data);

			nodeStack.pop();

			if (node->Right)nodeStack.push(node->Right);
			if (node->Left)nodeStack.push(node->Left);

		}//if
	}//if
	return *this;
}//operator=

template<typename T>
AVLTree<T>::AVLTree(AVLTree<T> &&other)noexcept
	:Root(other.Root), NumNodes(other.NumNodes)
{
	other.Root = nullptr;
	other.NumNodes = 0;
}//move constructor

template<typename T>
AVLTree<T> &AVLTree<T>::operator=(const AVLTree<T> &&other) noexcept
{
	if (this != &other)
	{
		DeleteTree();
		Root = other.Root;
		NumNodes = other.NumNodes;
		other.Root = nullptr;
		other.NumNodes = 0;
	}//if
}//move operator

template<typename T>
inline bool AVLTree<T>::Add(const T &newData)noexcept
{
	Node *newNode = new(std::nothrow) Node(newData);
	if (isEmpty()) Root = newNode;
	else
	{
		Node *Parent = nullptr;
		Node *tmp2 = nullptr;
		Node *tmp = Root;
		
		do
		{
			tmp2 = Parent;
			Parent = tmp;
			
			if (newData == Parent->Data) return false;
			else if (newData > Parent->Data) tmp = tmp->Right;
			else tmp = tmp->Left;

		} while (tmp != nullptr);
		
		if (newData > Parent->Data) Parent->Right = newNode;
    	else Parent->Left = newNode;
	
		SearchNodeToRebalance(Root);
	}//if
	NumNodes++;
	return true;
}//Add

template<typename T>
inline bool AVLTree<T>::DeleteTree(Node *node)noexcept
{
	if (node != nullptr)
	{
		DeleteTree(node->Left);
		DeleteTree(node->Right);
		delete node;
		Root = nullptr;
		NumNodes = 0;
	}
	return true;
}//DeleteTree

template<typename T>
inline bool AVLTree<T>::DeleteTree()noexcept
{
	if (!isEmpty()) return DeleteTree(Root);
	else return true;//false;
}//DeleteTree

template<typename T>
inline bool AVLTree<T>::Compare(const AVLTree<T> &other)const noexcept
{
	if (this->NumNodes != other.NumNodes)return false;

	vector<T> ArrayTree1;
	vector<T> ArrayTree2;
	ArrayTree1 = this->ToArray();
	ArrayTree2 = other.ToArray();

	for (size_t i = 0; i < ArrayTree1.size(); i++)
	{
		if (ArrayTree1[i] != ArrayTree2[i]) return false;
	}//for
	return true;
}//Compare

template<typename T>
inline bool AVLTree<T>::operator==(const AVLTree<T> &other) const noexcept
{
	if (this->NumNodes != other.NumNodes)return false;

	vector<T> ArrayTree1;
	vector<T> ArrayTree2;
	ArrayTree1 = this->ToArray();
	ArrayTree2 = other.ToArray();

	for (size_t i = 0; i < ArrayTree1.size(); i++)
	{
		if (ArrayTree1[i] != ArrayTree2[i]) return false;
	}//for
	return true;
}//operator==

template<typename T>
inline bool AVLTree<T>::isEmpty()const noexcept
{
	return(NumNodes == 0);
}//isEmpty

template<typename T>
inline bool AVLTree<T>::SearchNode(const T &Value)const noexcept
{
	if (isEmpty()) return false;
	else
	{
		if (Root->Data == Value) return true;
		else
		{
			Node *Parent = nullptr;
			Node *tmp = Root;
			while (tmp != nullptr)
			{
				Parent = tmp;
				if (Value == Parent->Data) return true;
				else if (Value > Parent->Data)tmp = tmp->Right;
				else tmp = tmp->Left;
			}//while
		}//if
	}//if
    return false;
}//Search Node

template<typename T>
inline bool AVLTree<T>::DeleteNode(const T &data)noexcept
{
	if (Root == nullptr) return false;
	else return DeleteNode(Root, data);
}//Delete Node

template<typename T>
inline typename AVLTree<T>::Node* AVLTree<T>::DeleteNode(Node* Root, T Item) noexcept
{
	Node* Tmp;
	if (Root == nullptr)
	{
		// Element not found.
		return (nullptr);
	}
	else if (Item < Root->Data)
	{
		// Recurse left.
		Root->Left = DeleteNode(Root->Left, Item);
	}
	else if (Item > Root->Data)
	{
		// Recurse right.
		Root->Right = DeleteNode(Root->Right, Item);
	}
	else if ((Root->Left != nullptr) && (Root->Right != nullptr))
	{
		// Element found and we have two sub-trees.
		Tmp = MinValue(Root->Right); // Replace T with...
		Root->Data = Tmp->Data; // ... its successor
		Root->Right = DeleteNode(Root->Right, Root->Data);

	}
	else
	{
		// Element found and we have one sub-tree.
		Tmp = Root;

		if (Root->Left == nullptr) // Handle 0 children
			Root = Root->Right;
		else if (Root->Right == nullptr)
			Root = Root->Left;

		if (Tmp == this->Root)this->Root = Root;
		free(Tmp); // Release node'
		NumNodes--;
		SearchNodeToRebalance(Root);
	} // if
	return (Root);
}//delete Node

template<typename T>
inline bool AVLTree<T>::AVLBalance()noexcept
{
	if (isEmpty())return false;
	else
	   return(AVLBalance(Root));
}//AVLBalance
template<typename T>
inline bool AVLTree<T>::SearchNodeToRebalance(Node* node)  noexcept
{
	if (node != nullptr)
	{
		if ((BalanceFactor(node) <= -2) || (BalanceFactor(node) >= 2))
		{
			if ((BalanceFactor(node->Right) <= -2) || (BalanceFactor(node->Right) >= 2))
			{
				SearchNodeToRebalance(node->Right);
			}
			else if ((BalanceFactor(node->Left) <= -2) || (BalanceFactor(node->Left) >= 2))
			{
				SearchNodeToRebalance(node->Left);
			}
			else { return AVLBalance(node); }
		}//if
	}//if
	return false;
}//SearchNodeToRebalance

template<typename T>
inline bool AVLTree<T>::AVLBalance(Node* node) noexcept
{
	if (node != nullptr)
	{
		Node* tmp = node->Right;
		Node* tmp2 = node->Left;

		if (BalanceFactor(node) >= 2)
		{
			if (BalanceFactor(tmp) >= 1)
			{
				LeftLeft(node); //Perform Single Left rotation
			}
			else
			{
				RightLeft(node); //Perform Double Left Rotation
			}//if
		}//if
		else if (BalanceFactor(node) <= -2)
		{
			if (BalanceFactor(tmp2) == 1)
			{
				LeftRight(node);// Perform Double Right rotation
			}
			else
			{
				RightRight(node);// Perform Single Right rotation
			}//if
		}//if
		AVLBalance(node->Left);
		AVLBalance(node->Right);
		return true;
	}//if
	return false;
}//AVLBalance

template<typename T>
inline size_t AVLTree<T>::GetItemCount()const noexcept
{
	return NumNodes;
}//GetItemCount

template<typename T>
inline size_t AVLTree<T>::Height()const noexcept
{
	return Height(Root);
}//Height

template<typename T>
inline size_t AVLTree<T>::Height(Node *node)const noexcept
{
	if (node == nullptr) return 0;

	size_t HeightLeft = Height(node->Left);
	size_t HeightRight = Height(node->Right);

	if (HeightLeft > HeightRight)return ++HeightLeft;
	else return ++HeightRight;
}//Height

template<typename T>
inline size_t AVLTree<T>::NodeCount()const noexcept
{
	size_t count = 0;
	if (Root != nullptr) count = NodeCount(Root);

	return count;
}//Node Count

template<typename T>
inline size_t AVLTree<T>::NodeCount(Node *node)const noexcept
{
	size_t count = 1;

	if (node->Left != nullptr) count += NodeCount(node->Left);

	if (node->Right != nullptr)count += NodeCount(node->Right);

	return count;
}//Node Count

template<typename T>
inline void AVLTree<T>::LeftLeft(Node* node) noexcept
{
	if (node != nullptr)
	{
		Node* tmp = node->Right;
		Node* tmp2 = tmp->Left;

		std::swap(*node, *tmp);
		node->Left = tmp;
		tmp->Right = tmp2;
	}//if
}//LeftRotation

template<typename T>
inline void AVLTree<T>::RightRight(Node* node) noexcept
{
	if (node != nullptr)
	{
		Node* tmp = node->Left;
		Node* tmp2 = tmp->Right;

		std::swap(*node, *tmp);
		node->Right = tmp;
		tmp->Left = tmp2;
	}//if
}//Right Rotation

template<typename T>
inline void AVLTree<T>::LeftRight(Node* node) noexcept
{
	if (node != nullptr)
	{
		Node* tmp = node->Left;
		Node* tmp2 = tmp->Right;
		Node* help = tmp2->Left;

		node->Left = tmp2;
		tmp2->Left = tmp;
		tmp->Right = help;

		RightRight(node);
	}//if
}//LeftRight

template<typename T>
inline void AVLTree<T>::RightLeft(Node* node) noexcept
{
	if (node != nullptr)
	{
		Node* tmp = node->Right;
		Node* tmp2 = tmp->Left;
		Node* help = tmp2->Left;

		node->Right = tmp2;
		tmp2->Right = tmp;
		tmp->Left = help;

		LeftLeft(node);
	}//if
}//RightLeft

template<typename T>
inline int AVLTree<T>::BalanceFactor(Node * node) const noexcept
{
	if (node == nullptr)return 0;
	else 
	{ 
		return(int)(Height(node->Right) - Height(node->Left)); 
	}//if
}//BalanceFactor

template<typename T>
inline void AVLTree<T>::PreOrder()const noexcept
{
	if (Root != nullptr) PreOrder(Root);
}//PreOrder

template<typename T>
inline void AVLTree<T>::PreOrder(Node* node)const noexcept
{
	if (node != nullptr)
	{
		cout << node->Data << " ";
		PreOrder(node->Left);
		PreOrder(node->Right);
	}//if
}//PreOrder

template<typename T>
inline void AVLTree<T>::PostOrder()const noexcept
{
	if (Root != nullptr) PostOrder(Root);
}//PostOrder

template<typename T>
inline void AVLTree<T>::PostOrder(Node *node)const noexcept
{
	if (node != nullptr)
	{
		PostOrder(node->Left);
		PostOrder(node->Right);
		cout << node->Data << " ";
	}//if
}//PostOrder

template<typename T>
inline void AVLTree<T>::InOrder()const noexcept
{
	if (Root != nullptr)InOrder(Root);
}//InOrder

template<typename T>
inline void AVLTree<T>::InOrder(Node *node)const noexcept
{
	if (node != nullptr)
	{
		InOrder(node->Left);
		cout << node->Data << " ";
		InOrder(node->Right);
	}//if
}//InOrder

template<typename T>
inline void AVLTree<T>::LevelOrder() const noexcept
{
	if (!isEmpty()) { LevelOrder(Root); }
}//LevelOrder

template<typename T>
inline void AVLTree<T>::LevelOrder(Node* node) const noexcept
{
	queue<Node*> q;
	Node* temp;
	q.push(Root);
	while (!q.empty())
	{
		temp = q.front();
		q.pop();
		cout << temp->Data << " ";
		if (temp->Left)
			q.push(temp->Left);
		if (temp->Right)
			q.push(temp->Right);
	}//while
}//LevelOrder

template<typename T>
inline std::vector<T> AVLTree<T>::ToArray() const noexcept
{
	vector<T> ArrayData;
	stack<Node*> nodeStack;

	nodeStack.push(Root);

	while (nodeStack.empty() == false)
	{
		Node *node = nodeStack.top();
		ArrayData.push_back(node->Data);

		nodeStack.pop();

		if (node->Right)nodeStack.push(node->Right);
		if (node->Left)nodeStack.push(node->Left);
	}
	return std::vector<T>(ArrayData);
}//ToArray

template<typename T>
inline bool AVLTree<T>::FromArray(const std::vector<T>& v) noexcept
{
	if (!v.empty())
	{
		for (size_t i = 0; i < v.size(); i++)
		{
			Add(v[i]);
		}//for 
		return true;
	}//if
	return false;
}//FromArray

template<typename T>
inline T AVLTree<T>::MinValue() const noexcept
{
	if (isEmpty())return false;

	Node* tmp = Root;
	T Min = tmp->Data;

	do
	{
		if (Min > tmp->Data) Min = tmp->Data;
		tmp = tmp->Left;
	} while (tmp != nullptr);

	return Min;
}//MinValue

template<typename T>
inline typename AVLTree<T>::Node* AVLTree<T>::MinValue(Node* Root) const noexcept
{
	if (isEmpty())return nullptr;

	Node* tmp = Root;
	Node* Min = tmp;

	do
	{
		if (Min->Data > tmp->Data) Min = tmp;/*= tmp->Data;*/
		tmp = tmp->Left;
	} while (tmp != nullptr);
	//std::cout << Min->Data;
	return Min;
}

template<typename T>
inline T AVLTree<T>::MaxValue() const noexcept
{
	if (isEmpty())return false;

	Node* tmp = Root;
	T Max = tmp->Data;

	do
	{
		if (Max < tmp->Data) Max = tmp->Data;
		tmp = tmp->Right;
	} while (tmp != nullptr);
	return Max;
} //MaxValue

template<typename T>
inline typename AVLTree<T>::Node* AVLTree<T>::MaxValue(Node* Root) const noexcept
{
	if (isEmpty())return nullptr;

	Node* tmp = Root;
	Node* Max = tmp;

	do
	{
		if (Max->Data < tmp->Data) Max = tmp;/* = tmp->Data;*/
		tmp = tmp->Right;
	} while (tmp != nullptr);
	//cout << Max->Data;
	return Max;
} //MaxValue

template<typename T>
inline bool AVLTree<T>::isFull() const noexcept
{
	return (NumNodes == (pow(2, Height()) - 1));
}//isFull 

//DeleteNode iterative
/*template<typename T>
inline bool AVLTree<T>::DeleteNode(const T& data)noexcept
{
	if (isEmpty())return false;
	else
	{
		Node* tmp = Root;
		Node* Parent = Root;
		Node* temp = Root;

		do
		{
			temp = Parent;
			Parent = tmp;
			if (data == tmp->Data)
			{
				if (tmp->IsLeafNode())
				{
					if (temp->Left == tmp)
					{
						delete tmp;
						temp->Left = nullptr;
						NumNodes--;
						return true;
					}
					else if (temp->Right == tmp)
					{
						delete tmp;
						temp->Right = nullptr;
						NumNodes--;
						return true;
					}
					else
					{
						DeleteTree();
						return true;
					}//if
				}//if

				if (tmp->HasOneChild())
				{
					if (temp->Left == tmp)
					{
						if (tmp->Left != nullptr)
						{
							temp->Left = tmp->Left;
							delete tmp;

							NumNodes--;
							AVLBalance();////
							return true;
						}
						else if (tmp->Right != nullptr)
						{
							temp->Left = tmp->Right;
							delete tmp;
							NumNodes--;
							AVLBalance();////
							return true;
						}//if
						else return false;
					}//if
					else if (temp->Right == tmp)
					{
						if (tmp->Left != nullptr)
						{
							temp->Right = tmp->Left;
							delete tmp;
							NumNodes--;
							AVLBalance();////
							return true;
						}
						else if (tmp->Right != nullptr)
						{
							temp->Right = tmp->Right;
							delete tmp;
							NumNodes--;
							AVLBalance();////
							return true;
						}//if
						else return false;
					}
					else if (tmp == temp)
					{
						if (tmp->Left != nullptr)
						{
							Root = tmp->Left;
							delete tmp;
							NumNodes--;
							AVLBalance();////
							return true;
						}
						else
						{
							Root = tmp->Right;
							delete tmp;
							NumNodes--;
							AVLBalance();////
							return true;
						}//if
					}//if
				}//if

				if (tmp->HasTwoChildren())
				{
					if (Parent->Data < Root->Data)
					{
						temp = Parent;
						tmp = Parent->Left;

						while (tmp->Right != nullptr)
						{
							temp = tmp;
							tmp = tmp->Right;
						}//while

						Parent->Data = tmp->Data;

						if (temp->Left == tmp)
						{
							temp->Left = tmp->Left;
						}
						else
						{
							temp->Right = tmp->Left;
						}

						delete tmp;
						NumNodes--;
						AVLBalance();////
						return true;
					}
					else if (Parent->Data >= Root->Data)
					{
						temp = Parent;
						tmp = Parent->Right;

						while (tmp->Left != nullptr)
						{
							temp = tmp;
							tmp = tmp->Left;
						}//while

						Parent->Data = tmp->Data;

						if (temp->Right == tmp)
						{
							temp->Right = tmp->Right;
						}
						else
						{
							temp->Left = tmp->Right;
						}
						delete tmp;
						NumNodes--;
						AVLBalance();////
						return true;
					}//if
					else return false;
				}//if

			}//if
			else if (data > tmp->Data)tmp = tmp->Right;
			else tmp = tmp->Left;
		} while (tmp != nullptr);
	}//if
	return false;
}//Delete Node*/